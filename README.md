# jQuery linkIt Plugin #

Simple jQuery plugin that allows you to attach a link to any element. From Eduonix's [**Projects In JavaScript & JQuery course**](https://stackskills.com/courses/projects-in-javascript-jquery) on **Stackskills**.