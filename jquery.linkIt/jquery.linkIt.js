/*
* name:    linkIt
* author:  Angie Siu
* version: 0.1.0
* license: MIT
*/

(function($){
    $.fn.linkIt = function(options) {
        //default settings
        var settings = $.extend({
            href: null, //allow user to add this in so set init value to null    
            text: null,
            target: '_self'
        }, options);
        
        //validate
        if(settings.href == null) {
            console.log('You need an href option for linkIt to work.');
            return this;
        }
        
        return this.each(function() {//for each element that you attach this plugin to
            var object = $(this); //set object as all elements with plugin attached
            
            if(settings.text == null) {
                settings.text = object.text(); //if text prop not set, then use object's text    
            }
            
            object.wrap('<a target="'+settings.target+'" href="'+settings.href+'"></a>').text(settings.text); //wrap the objects with the <a> tag and insert the text
        });
        
        
    }
}(jQuery));